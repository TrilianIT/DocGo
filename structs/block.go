package structs

type Block struct {
	Header  string
	Content []BlockContent
}
